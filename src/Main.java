package com.company;

import javax.sound.midi.Soundbank;
import java.util.Random;

public class Main {

    public static void main(String[] args) {
        Random random = new Random();

        System.out.println("Bem-vindo ao jogo de dados do Vinicius!");
        System.out.println("Item 1 - Sortear 1 numero");

        int numeroDado = random.nextInt(6);

        System.out.println(numeroDado);
        System.out.println(" ");

        System.out.println("Item 2 - Sortear 3 numeros e a soma deles");

        int soma = 0;

        for (int i = 0; i<3; i++){
            int numeroDado2 = random.nextInt(6);
            System.out.print(numeroDado2 + ", ");
            soma += numeroDado2;
        }
        System.out.println(soma);
        System.out.println(" ");

        System.out.println("Item 3 - Sortear 3 grupos de 3 numeros e a soma deles");

        for(int x = 0; x < 3; x++){

                int soma2 = 0;

                for (int y = 0; y < 3; y++){
                    int numeroDado3 = random.nextInt(6);
                    System.out.print(numeroDado3 + ", ");
                    soma2 += numeroDado3;
                }
                System.out.println(soma2);
            }
        }
    }
